#include <SoftwareSerial.h>
#include <Arduino.h>
#include <DFPlayerMiniWrapperAsync.h>

#include <Wire.h>
#include <RTClib.h>

// Pin constants
static const int gc_pinDfPlayerSerialTx = D6;
static const int gc_pinDfPlayerSerialRx = D5;
static const int gc_pinRtcScl = D1;
static const int gc_pinRtcSda = D2;

// Setting constants
static const int gc_volume = 20;
static const int gc_deepSleepTimeAfterPlaing = 5000;

SoftwareSerial g_mp3Serial(gc_pinDfPlayerSerialRx, gc_pinDfPlayerSerialTx);
DFPlayerMiniWrapper g_mp3Module;
RTC_DS1307 g_rtc;
TwoWire g_twoWireRtc;

void setup()
{
    Serial.begin(115200); // Standard hardware serial port
    Serial.println("Audio Advent Calendar by JohnWalkerx");

    // Initalize MP3-Module
    g_mp3Serial.begin(9600);
    delay(600);
    g_mp3Module.setDebugSerial(Serial);

    Serial.println("Initialize mp3-Module");
    g_mp3Module.begin(g_mp3Serial);
    delay(500);
    g_mp3Module.setVolume(gc_volume);
    g_mp3Module.stop();

    // Initalize RTC
    g_twoWireRtc.begin(gc_pinRtcSda, gc_pinRtcScl);
    g_rtc.begin(&g_twoWireRtc);
    delay(500);

    // If RTC is not running, set compile-time time.
    if (!g_rtc.isrunning())
    {
        Serial.println("RTC is NOT running!");
        // following line sets the RTC to the date & time this sketch was compiled
        g_rtc.adjust(DateTime(__DATE__, __TIME__));
    }
}

void loop()
{
    static bool playStarted{false};
    static int playStartedTime{-1};
    g_mp3Module.processCommunication();

    DateTime now = g_rtc.now();

    static int lastPrint = millis();
    if (millis() - lastPrint > 1000)
    {
        lastPrint = millis();
        Serial.println("Current Date: " + now.timestamp());
    }

    // After playing is started, put ESP into deep sleep mode because the mp3 module stops playing after one track automatically.
    // To start playing again, power connection will be reconnected.
    if (playStarted && (millis() - playStartedTime > gc_deepSleepTimeAfterPlaing))
    {
        Serial.println("Enter deep-sleep mode....");
        ESP.deepSleep(ESP.deepSleepMax());
    }

    if (playStarted)
    {
        return;
    }

    if (now.month() == 12 && now.day() < 25)
    {

        Serial.println("Started playing day: " + String(now.day()));
        g_mp3Module.play(now.day());
    }
    // Not an advent day
    else
    {
        Serial.println("Not an advent day. Play message");
        g_mp3Module.play(25);
    }

    playStarted = true;
    playStartedTime = millis();
}
