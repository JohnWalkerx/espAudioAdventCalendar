# espAudioAdventCalendar

[![status-badge](https://ci.codeberg.org/api/badges/JohnWalkerx/espAudioAdventCalendar/status.svg)](https://ci.codeberg.org/JohnWalkerx/espAudioAdventCalendar)

This is an audio advent calender with an ESP8266.

It plays every day from 1st to 24th december a specific mp3 file.
You can hear the track of the day multiple times.
If it's another day it plays a track which informs the user that there is
no story for today.

With the realtime clock it doesn't need any internet connection. If the RTC is not running it uses the compile time to set the RTC.

The ESP and MP3 module uses a 9V battery as power supply.

You can find pictures [down below](#pictures).

Inspired by [mischk's audioadventcalendar_ng](https://github.com/mischk/audioadventcalendar_ng).

## Build

Clone the repository `git clone <remote-repo-url>`, change directory `cd espAudioAdventCalender` and initialize the submodules with `git submodule update --init`.

Install [PlatformIO](https://platformio.org/) extension in VSCode to compile and upload it to the ESP8266.

## Circuit diagram

You can find the circuit diagram [here](doc/espAudioAdventCalender.pdf).

## Used components

It uses following components:

- [ESP8266 Wemos D1 mini (clone)](https://www.berrybase.de/dev.-boards/esp8266-esp32-d1-mini/boards/d1-mini-esp8266-entwicklungsboard)
- [DFRobot DFPlayer mini](https://www.berrybase.de/sensoren-module/audio-schall/mp3-player-modul-mit-eingebautem-verst-228-rker)
- [DS1307 realtime clock](https://www.berrybase.de/neu/ds1307-realtime-clock/echtzeituhr-shield-f-252-r-d1-mini?c=2474)
- [Mono speaker](https://www.berrybase.de/sensoren-module/audio-schall/mono-geh-228-uselautsprecher-3w-4-ohm)
- [Pololu 5V, 1A Step-Down D24V10F5](https://www.berrybase.de/sensoren-module/strom-spannung/pololu-5v-1a-step-down-spannungsregler-d24v10f5)
- [9V battery holder](https://www.berrybase.de/strom/batterien-akkus/installation-zubehoer/batterie-clips-halter/batteriehalter-fuer-9v-block/batteriehalter-f-252-r-1x-blockbatterie-9-v-mit-l-246-tanschluss)
- [Micro switch](https://www.berrybase.de/bauelemente/schalter-taster/mikroschalter-taster/mikroschalter-mit-hebel-printmontage-1a/250vac-on-40-on-41-1-stellig)
- [LED 5mm red](https://www.berrybase.de/bauelemente/aktive-bauelemente/leds/low-current-leds/kingbright-low-current-led-5mm-rot)
- microSDHC card for DFPlayer mini

I used a little case and mounted everything inside.

## Prepare SD card for MP3-Player

You need to place the mp3-file in the root directory of the SD card. It needs to be formated with fat32.
The file have to be names like `0001.mp3, 0002.mp3, ...`.
If there is not advent day, the device will play the file `0025.mp3`. Which is in my case a recording which tells that there is no story for today.
So the user knows that the device is still working.

## License

The source code is licensed under the [MIT Licence](LICENCE).
All pictures and documentation are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License.](https://creativecommons.org/licenses/by-sa/4.0/).

## Pictures

![Picture of complete box](doc/assets/completeBox.jpg)

![Picture of opened box](doc/assets/openBox.jpg)

![Picture of circuit board](doc/assets/circuitBoard.jpg)

![Picture of circuit board bottom](doc/assets/circuitBoardButtom.jpg)
